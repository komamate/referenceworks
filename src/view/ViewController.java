package blokkzaro2.view;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import blokkzaro2.entities.Projects;
import blokkzaro2.entities.Tasks;
import blokkzaro2.entities.Users;
import blokkzaro2.repo.Repository;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ViewController implements Initializable {
	
	@FXML
	private Pane userPane, taskPane, projectPane;
	@FXML
	private TableView<Tasks> taskView;
	@FXML
	private TableView<Users> userView;
	@FXML
	private TableView<Projects> projectView;
	@FXML
	private ComboBox<Projects> projectBox;
	
	private List<Tasks> tasks;
	private List<Users> users;
	private List<Projects> projects;
	private blokkzaro2.repo.Repository repo;

	
	public void onSelect(ActionEvent event) {
		Object source = event.getSource();
		Projects projects = ((ComboBox<Projects>) source).getValue();

		// a kiv�lasztott elemmel megh�vom a refreshTable met�dust, �s friss�tem a
		// t�bl�t
		refreshTable(projects);
	}

	private void refreshTable(Projects projects) {
		taskView.setItems(FXCollections.observableArrayList(repo.listByProjects(projects)));
	}

	public void initialize(URL location, ResourceBundle resources) {
		initPanels(true);// indításkor az üdvözlő panel látszódjon

		// inicializálja a Repot
		repo = new Repository();
		users = repo.findAllUsers();
		projects = repo.findAllProjects();
		
		initUserTable();
		initUsersView();
		initProjectsCombobox();
		initTaskTable();
		initProjectView();
		
		
		
	}

	private void initPanels(boolean visible) {
		userPane.setVisible(visible);
		taskPane.setVisible(visible);
		projectPane.setVisible(visible);
	}
	
	public void addUser(ActionEvent event) {
		Stage popupwindow2=new Stage();
	      
		popupwindow2.initModality(Modality.APPLICATION_MODAL);
		popupwindow2.setTitle("Add new User");
		      
		      
		Label label1= new Label("User ID");
		     
		TextField idField = new TextField();
		
		Label label2 = new Label("User name");
		
		TextField nameField = new TextField();
		
		Label label3 = new Label("User email");
		     
		TextField emailField = new TextField();
		
		Button button1= new Button("Save User");	
		     
		Button button2 = new Button("Close");
		button2.setOnAction(e -> popupwindow2.close());
		button1.setOnAction(e -> {
			Users user =new Users(emailField.getText(),Integer.parseInt(idField.getText()),nameField.getText());
			repo.save(user);
			users.add(user);
			initUsersView();
			popupwindow2.close();});
		     
		     

		VBox layout= new VBox(10);
		     
		      
		layout.getChildren().addAll(label1,idField,label2,nameField,label3,emailField,button1,button2);
		      
		layout.setAlignment(Pos.CENTER_LEFT);
		      
		Scene scene1= new Scene(layout, 300, 250);
		      
		popupwindow2.setScene(scene1);
		      
		popupwindow2.showAndWait();
	}

	public void addTask(ActionEvent event2) {
		Stage popupwindow=new Stage();
	      
		popupwindow.initModality(Modality.APPLICATION_MODAL);
		popupwindow.setTitle("Add new Task");
		      
		      
		Label label1= new Label("ID");
		     
		TextField idField = new TextField();
		
		Label label2 = new Label("Owner ID");
		
		TextField ownerIdField = new TextField();
		
		Label label3 = new Label("Project ID");
		     
		TextField projectIdField = new TextField();
		
		Label label4 = new Label("Task Name");
		
		TextField taskNameField = new TextField();
		
		Button button1= new Button("Save Task");	
		     
		Button button2 = new Button("Close");
		button2.setOnAction(e -> popupwindow.close());
		button1.setOnAction(e -> {
			Tasks task =new Tasks(Integer.parseInt(idField.getText()),Integer.parseInt(ownerIdField.getText()),Integer.parseInt(projectIdField.getText()),taskNameField.getText());
			repo.saveTasks(task);
			tasks.add(task);
			initTasksView();
			popupwindow.close();});
		     
		     

		VBox layout2= new VBox(10);
		     
		      
		layout2.getChildren().addAll(label1,idField,label2,ownerIdField,label3,projectIdField,label4,taskNameField,button1,button2);
		      
		layout2.setAlignment(Pos.CENTER_LEFT);
		      
		Scene scene2= new Scene(layout2, 300, 250);
		      
		popupwindow.setScene(scene2);
		      
		popupwindow.showAndWait();

	}

	public void addProject(ActionEvent event3) {
		Stage popupwindow=new Stage();
	      
		popupwindow.initModality(Modality.APPLICATION_MODAL);
		popupwindow.setTitle("Add new User");
		      
		      
		Label label1= new Label("Created By");
		     
		TextField createdByField = new TextField();
		
		Label label2 = new Label("End Date");
		
		TextField endDateField = new TextField();
		
		Label label3 = new Label("ID");
		     
		TextField idField = new TextField();
		
		Label label4 = new Label("Project Name");
		
		TextField projectNameField = new TextField();
		
		Label label5 =new Label("Start Date");
		
		TextField startDateField = new TextField();
		
		Button button1= new Button("Save Project");	
		     
		Button button2 = new Button("Close");
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		 button2.setOnAction(e -> popupwindow.close());
		button1.setOnAction(e -> {
			
			String startDateString = startDateField.getText();
			String endDateString = endDateField.getText();
			Date startdate;
			Date enddate;
			try {
				startdate = sdf.parse(startDateString);
				enddate = sdf.parse(endDateString);
				System.out.println(startdate);
				System.out.println(enddate);
				Projects project =new Projects(Integer.parseInt(createdByField.getText()),enddate,Integer.parseInt(idField.getText()),projectNameField.getText(),startdate);
				repo.saveProjects(project);
				projects.add(project);
				initProjectView();
				popupwindow.close();
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
			});
		     
		     

		VBox layout3= new VBox(10);
		     
		      
		layout3.getChildren().addAll(label1,createdByField,label2,endDateField,label3,idField,label4,projectNameField,label5,startDateField,button1,button2);
		      
		layout3.setAlignment(Pos.CENTER_LEFT);
		      
		Scene scene3= new Scene(layout3, 300, 250);
		      
		popupwindow.setScene(scene3);
		      
		popupwindow.showAndWait();
	}

	private void initProjectsCombobox() {
		// ha esetleg nem tudja @FXML annotációval elérni az adott elemünket
		// akkor:
		// cityCombobox = (ComboBox) cityPane.lookup("#cityCombobox");

		// combobox feltöltése:
		projectBox.getItems().addAll(repo.findAllProjects());
		projectBox.setVisible(true);
		projectBox.setVisibleRowCount(3); // alapértelmezetten mennyi elem
											// jelenjen meg, a többi gördíthető
	}

	private void initUsersView() {
		userView.getItems().clear();
		userView.getItems().addAll(users);
		userView.setVisible(true);
		
	}
	private void initTasksView() {
		taskView.getItems().clear();
		taskView.getItems().addAll(tasks);
		taskView.setVisible(true);
		
	}
	private void initProjectView() {
		projectBox.getItems().clear();
		System.out.println(projects);
		projectBox.getItems().addAll(projects);
		projectBox.setVisible(true);
	}
	private void initUserTable() {
		TableColumn<Users, String> emailCol = new TableColumn<>("Email");
		emailCol.setCellFactory(TextFieldTableCell.forTableColumn());
		emailCol.setCellValueFactory(new PropertyValueFactory<Users, String>("email"));
		emailCol.setMinWidth(200);

		TableColumn<Users, String> nameCol = new TableColumn<>("N�v");
		nameCol.setCellFactory(TextFieldTableCell.forTableColumn());
		nameCol.setCellValueFactory(new PropertyValueFactory<Users, String>("name"));
		nameCol.setMinWidth(200);

		TableColumn<Users, Integer> proId = new TableColumn<>("UserID");
		proId.setCellValueFactory(data -> new SimpleIntegerProperty(data.getValue().getId()).asObject());
		proId.setMinWidth(200);

		userView.getColumns().addAll(emailCol, nameCol, proId);

	}

	private void initTaskTable() {

		TableColumn<Tasks, String> nameCol = new TableColumn<>("Project N�v");
		nameCol.setCellFactory(TextFieldTableCell.forTableColumn());
		nameCol.setCellValueFactory(new PropertyValueFactory<Tasks, String>("project_name"));
		nameCol.setMinWidth(200);

		TableColumn<Tasks, Integer> idCol = new TableColumn<>("Id");
		idCol.setCellFactory(col -> new TableCell<Tasks, Integer>());
		idCol.setCellValueFactory(data -> new SimpleIntegerProperty(data.getValue().getId()).asObject());
		idCol.setMinWidth(200);

		TableColumn<Tasks, Integer> proId = new TableColumn<>("Projekt ID");
		proId.setCellFactory(col -> new TableCell<Tasks, Integer>());
		proId.setCellValueFactory(data -> new SimpleIntegerProperty(data.getValue().getProject_id()).asObject());
		proId.setMinWidth(200);

		TableColumn<Tasks, Integer> ownerId = new TableColumn<>("Owner ID");
		ownerId.setCellValueFactory(data -> new SimpleIntegerProperty(data.getValue().getOwner_id()).asObject());
		ownerId.setMinWidth(200);

		// hozzáadjuk az oszlopokat a táblázathoz
		taskView.getColumns().addAll(nameCol, idCol, proId, ownerId);
	}
	
}
