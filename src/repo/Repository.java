package blokkzaro2.repo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import blokkzaro2.entities.Projects;
import blokkzaro2.entities.Tasks;
import blokkzaro2.entities.Users;


public class Repository {

	private final String URL = "jdbc:mysql://localhost:3306/projectplanner";
	private final String USERNAME = "root";
	private final String PW = "";
	

	private Connection con = null;
	private Statement statement = null;

	public Repository() {
		initDatabase();
	}

	//kapcsolat felépítése
	private void initDatabase() {
		try {

			con		  = DriverManager.getConnection(URL, USERNAME, PW);
			statement = con.createStatement();

		} catch (Exception ex) {
			System.out.println("Hiba a kapcsolat ki�p�t�se \n " + ex);
		}
	}

	public void close() {
		try {
			if (con != null) {
				con.close();
			}
		} catch (Exception ex) {
			System.out.println("Nem siker�lt z�rni a kapcsolatot \n" + ex);
		}
	}


	public List<Projects> findAllProjects() {
		String query = "SELECT * FROM project";
		List<Projects> projects = new ArrayList<>();

		try {
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				//úgy is lehetne, hogy setterrel állítunk be mindent
//				City city = new City();
//				city.setCityName(rs.getString("nev"));
//				
				Projects project = new Projects(rs.getInt("created_by"),
						rs.getDate("end_date"), rs.getInt("id"),
						rs.getString("project_name"),
						rs.getDate("start_date"));
				
				projects.add(project);
			}

		} catch (SQLException ex) {
			System.out.println("Hiba a projektek lek�rdez�sekor \n " + ex);
		}

		return projects;
	}
	
	
	public List<Users> findAllUsers(){
		String query = "SELECT * from User";
		
		List<Users> userList = new ArrayList<>();
		try {

			
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				Users user = new Users(
						rs.getString("email"),
						rs.getInt("id"),
						rs.getString("name")
						);
				userList.add(user);
				
			}

		} catch (SQLException ex) {
			System.out.println("Hiba a felhaszn�l�k lek�rdez�sekor \n " + ex);
		}
		return userList;
	}
	public List<Tasks> listByProjects(Projects projects){
		String query = "SELECT * from task WHERE task.project_id =" + projects.getId();
		
		List<Tasks> taskList = new ArrayList<>();
		try {

			
			ResultSet rs = statement.executeQuery(query);
			while (rs.next()) {
				Tasks tasks = new Tasks(
						rs.getInt("id"),
						rs.getInt("owner_id"),
						rs.getInt("project_id"),
						rs.getString("task_name")
						);
				taskList.add(tasks);
			}

		} catch (SQLException ex) {
			System.out.println("Hiba a feladatok lek�rdez�sekor \n " + ex);
		}
		return taskList;
	}
	
	public void save(Users user) {
		String query = "Insert into user (email,name,id) values ('" + user.getEmail() + "','" + user.getName() + "'," + user.getId() + ")";
	try {
		statement.execute(query);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	
	public void saveTasks(Tasks task) {				
		String query = "Insert into task (id,owner_id,project_id,task_name) values (" + task.getId() + "," + task.getOwner_id() + "," + task.getProject_id() + ",'" + task.getTask_name() + "')";
	try {
		statement.execute(query);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	public void saveProjects(Projects project) {
		String query = "Insert into project (created_by,end_date,id,project_name,start_date) values ('" + project.getCreated_by() + "','" + project.getEnd_date() + "'," + project.getId() + ",'" + project.getProject_name() + "','" + project.getStart_date() + "')";
	try {
		statement.execute(query);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
}
