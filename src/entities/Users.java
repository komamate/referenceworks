package blokkzaro2.entities;

public class Users {
	private String email;
	private String name;
	private Integer id;
	
	public Users() {
		
	}
	
	
	
	public Users(String email,Integer id, String name) {
		super();
		this.email = email;
		this.name = name;
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}



	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Users [email=").append(email).append(", name=").append(name).append(", id=").append(id)
				.append("]");
		return builder.toString();
	}
	
}
