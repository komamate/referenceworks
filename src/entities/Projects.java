package entities;

import java.util.Date;

public class Projects {
	private Integer created_by; 
	private Date end_date; 
	private Integer id;
	private String project_name;
	private Date start_date;
	
	
	public Projects() {
		
	}
	
	public Projects(Integer created_by, Date end_date, Integer id, String project_name, Date start_date) {
		super();
		this.created_by = created_by;
		this.end_date = end_date;
		this.id = id;
		this.project_name = project_name;
		this.start_date = start_date;
	}
	public Integer getCreated_by() {
		return created_by;
	}
	public void setCreated_by(Integer created_by) {
		this.created_by = created_by;
	}
	public Date getEnd_date() {
		return end_date;
	}
	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getProject_name() {
		return project_name;
	}
	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}
	public Date getStart_date() {
		return start_date;
	}
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Projects [created_by=").append(created_by).append(", end_date=").append(end_date)
				.append(", id=").append(id).append(", project_name=").append(project_name).append(", start_date=")
				.append(start_date).append("]");
		return builder.toString();
	}
	
	
}
