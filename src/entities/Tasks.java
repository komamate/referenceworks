package blokkzaro2.entities;

public class Tasks {
	private Integer id;
	private Integer owner_id;
	private Integer project_id;
	private String task_name;
	
	public Tasks() {
		
	}
	
	public Tasks(Integer id, Integer owner_id, Integer project_id, String task_name) {
		super();
		this.id = id;
		this.owner_id = owner_id;
		this.project_id = project_id;
		this.task_name = task_name;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getOwner_id() {
		return owner_id;
	}
	public void setOwner_id(Integer owner_id) {
		this.owner_id = owner_id;
	}
	public Integer getProject_id() {
		return project_id;
	}
	public void setProject_id(Integer project_id) {
		this.project_id = project_id;
	}
	public String getTask_name() {
		return task_name;
	}
	public void setTask_name(String project_name) {
		this.task_name = project_name;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Tasks [id=").append(id).append(", owner_id=").append(owner_id).append(", project_id=")
				.append(project_id).append(", task_name=").append(task_name).append("]");
		return builder.toString();
	}
	
	
}
